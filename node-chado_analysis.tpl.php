<?php
// Purpose: This template provides the layout of the organism node (page)
//   using the same templates used for the various analysis content blocks.
//
// To Customize the Featture Node Page:
//   - This Template: customize basic layout and which elements are included
//   - Using Panels: Override the node page using Panels3 and place the blocks
//       of content as you please. This method requires no programming. See
//       the Tripal User Guide for more details
//   - Block Templates: customize the content/layout of each block of stock 
//       content. These templates are found in the tripal_stock subdirectory
//
// Variables Available:
//   - $node: a standard object which contains all the fields associated with
//       nodes including nid, type, title, taxonomy. It also includes stock
//       specific fields such as stock_name, uniquename, stock_type, synonyms,
//       properties, db_references, object_relationships, subject_relationships,
//       organism, etc.
//   NOTE: For a full listing of fields available in the node object the
//       print_r $node line below or install the Drupal Devel module which 
//       provides an extra tab at the top of the node page labelled Devel
?>

<?php
 //uncomment this line to see a full listing of the fields avail. to $node
 //print '<pre>'.print_r($variables,TRUE).'</pre>';

drupal_add_css('./tripal-node-templates.css');

$node = $variables['node'];
$organism = $variables['node']->organism;
?>

<?php if ($teaser) { 
  include('tripal_analysis/tripal_analysis_teaser.tpl.php'); 
} else { ?>

<script type="text/javascript">
// Adds all tripal expandable boxes to the top bar
Drupal.behaviors.myBehavior = function (context) {
   $(document).ready(function() {
      
      // iterate through all of the info boxes and add their titles
      // to the table of contents
      $(".tripal-info-box-title").each(function(key, fn){
        var parent = $(this).parent();
        var id = $(parent).attr('id');
        var title = $(this).text();
        var lastIndex = $(".tripal-info-box-title").size() -1;
        if (key == lastIndex) {
          $('#tripal_analysis-topbar-list').append('<li><a href="#'+id+'" class="tripal_analysis-topbar-item">'+title+'</a></li>');
        } else {
          $('#tripal_analysis-topbar-list').append('<li><a href="#'+id+'" class="tripal_analysis-topbar-item">'+title+'</a> &middot; </li>');
        }
      });
      
      // when a title in the table of contents is clicked, then
      // scroll to the corresponding item
      $(".tripal_analysis-topbar-item").click(function(){
         href = $(this).attr('href');
         document.$(href).scrollIntoView();
         return false;
      }); 

   });
}
</script>

<script type="text/javascript">
// Ensures the topbar always stays at the top of the screen
if (Drupal.jsEnabled) {
  $(document).ready(function() {
  
    // set the top section toolbar width based on it's placeholder
    $.setToolbarWidth = function() {
      var width = $(".tripal-topbar-placeholder").width();
      var inner = $(".inner").width();
      var paddingRight = $(".tripal-topbar").css('padding-right').replace("px", "");
      var paddingLeft = $(".tripal-topbar").css('padding-left').replace("px", "");
      var topbarWidth = width - paddingRight - paddingLeft;
      $(".tripal-topbar").css('width',topbarWidth);
    };
    var setToolbarWidth = $.setToolbarWidth();
    
    $.setToolbarTop = function() {
      var newTop = $(window).scrollTop();
      if (newTop > originalTop) {
        $(".tripal-topbar").css('top',0);
      } else if (newTop < originalTop) {
         bufferedTop = originalTop - newTop;
         $(".tripal-topbar").css('top',bufferedTop);
      }    
    }
    
    $.setToolbarLeft = function () {
      var placeholderLeft = $(".tripal-topbar-placeholder").offset().left;
      var pageLeft = $(window).scrollLeft();
      var newToolbarLeft = placeholderLeft - pageLeft;
      $(".tripal-topbar").css('left',newToolbarLeft);
    }
    
  	// Make sure it's max-width is set to that of the page
  	$(window).resize(setToolbarWidth);
    
  	// Part that scrolls the div toolbar
    var originalTop = $(".tripal-topbar-placeholder").offset().top;
    $(window).scroll(function() {
      $.setToolbarWidth();
      $.setToolbarTop();
      $.setToolbarLeft();
    });
  });
}
</script>

<!-- Table of contents -->
<div class="tripal-topbar-placeholder">
  <div id="tripal_analysis-topbar" class="tripal-topbar">
    <span id="tripal_analysis-topbar-title" class="tripal-topbar-title">Sections:</span>
    <ul id="tripal_analysis-topbar-list" class="tripal-topbar-list">
    </ul>
  </div>
</div>

<div id="tripal_analysis-node" class="tripal-node">

   <!-- Basic Details Theme -->
   <?php include('tripal_analysis/tripal_analysis_base.tpl.php'); ?>

   <?php print $content ?>
</div>

<?php } ?>
