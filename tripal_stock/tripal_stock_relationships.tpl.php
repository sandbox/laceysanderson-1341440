<?php
// Copyright 2010 University of Saskatchewan (Lacey-Anne Sanderson)
//
// Purpose: Provides layout and content for Stock Relationships where
//   the current stock is the Subject of the relationships. This includes all 
//   fields in the stock_relationship table.
//
// Note: This template controls the layout/content for the default stock node
//   template (node-chado_stock.tpl.php) and the Stock Object Relationships Block
//
// Variables Available:
//   - $node: a standard object which contains all the fields associated with
//       nodes including nid, type, title, taxonomy. It also includes stock
//       specific fields such as stock_name, uniquename, stock_type, synonyms,
//       properties, db_references, object_relationships, subject_relationships,
//       organism, etc.
//   - $node->object_relationships: an array of stock relaionship objects 
//       where each object has the following fields: stock_relationship_id,
//       subject_id (current stock_id), type_id, type, value, rank, object
//   - $node->object_relationships->object: a stock object describing the
//       object stock with the fields: stock_id, stock_name, uniquename, 
//       description, stock_type_id, organism(object), man_db_reference(object),
//       nid (if sync'd with Drupal)
//   NOTE: For a full listing of fields available in the node object the
//       print_r $node line below or install the Drupal Devel module which 
//       provides an extra tab at the top of the node page labelled Devel
?>

<?php


// expand the stock object to include the stock relationships.
// since there two foreign keys (object_id and subject_id) in the 
// stock_relationship table, we will access each one separately 
$node = tripal_core_expand_chado_vars($node,
   'table','stock_relationship', array('order_by'=>array('rank' => 'ASC')));

 //uncomment this line to see a full listing of the fields avail. to $node
 //print '<pre>'.print_r($node,TRUE).'</pre>';

  $srelationships = $node->stock->stock_relationship->subject_id;
  if (!$srelationships) {
    $srelationships = array();
  } elseif (!is_array($srelationships)) { 
    $srelationships = array($srelationships); 
  } 

  $orelationships = $node->stock->stock_relationship->object_id;
  if (!$orelationships) {
    $orelationships = array();
  } elseif (!is_array($orelationships)) { 
    $orelationships = array($orelationships); 
  }
?>

<?php if ($srelationships OR $orelationships) { ?>
<div id="tripal_stock-relationships-box" class="tripal_stock-info-box tripal-info-box"> 
  <div class="tripal_stock-info-box-title tripal-info-box-title">Relationships</div> 

  <table class="tripal_stock-table tripal-table tripal-table-horz">
    <tr>
      <th class="tripal-table-first-column">Subject</th>
      <th>Type</th>
      <th>Object</th>
    </tr>
  
    <?php 
    // Subjects -----------------------------------
    if(count($srelationships) > 0){ ?>
  
      <tr class="tripal-table-interupt"><td colspan="5">Subject Relationships</td></tr>
    
      <?php
      $i = 0; 
      $last = sizeof($srelationships) -1;
      foreach ($srelationships as $result){   
        $class = 'tripal_stock-table-odd-row tripal-table-odd-row';
        if($i % 2 == 0 ){
          $class = 'tripal_stock-table-odd-row tripal-table-even-row';
        } 
        if ($i == $last) {
          $class .= ' tripal-table-last-row';
        }
      
        $object = $result->object_id;
			  if ($object->nid) {
			    $object_link = l($object->name.' ('.$object->uniquename.')', 'node/'.$object->nid);
			  } else {
			    $object_link = $object->name.' ('.$object->uniquename.')';
			  }
			  $i++;
        ?>
      
        <tr class="<?php print $class ?>">
				  <td class="tripal-table-first-column"><?php print $node->stock->name; ?></td>
				  <td><?php print $result->type_id->name; ?></td>
				  <td><?php print $object_link; ?></td>
        </tr>
      <?php } //end of foreach?>
    <?php } ?>

    <?php 
    // Objects -----------------------------------
    if(count($orelationships) > 0){ ?>
  
      <tr class="tripal-table-interupt"><td colspan="5">Object Relationships</td></tr>
    
      <?php
      $i = 0; 
      $last = sizeof($orelationships) -1;
      foreach ($orelationships as $result){   
        $class = 'tripal_stock-table-odd-row tripal-table-odd-row';
        if($i % 2 == 0 ){
          $class = 'tripal_stock-table-odd-row tripal-table-even-row';
        } 
        if ($i == $last) {
          $class .= ' tripal-table-last-row';
        }
      
        $subject = $result->subject_id;
			  if ($subject->nid) {
			    $subject_link = l($subject->name.' ('.$subject->uniquename.')', 'node/'.$subject->nid);
			  } else {
			    $subject_link = $subject->name.' ('.$subject->uniquename.')';
			  }
			  $i++;
        ?>
      
        <tr class="<?php print $class ?>">
				  <td class="tripal-table-first-column"><?php print $subject_link; ?></td>
				  <td><?php print $result->type_id->name; ?></td>
				  <td><?php print $node->stock->name; ?></td>
        </tr>
      <?php } //end of foreach?>
    <?php } ?>  
  
  </table>
</div>
<?php } ?>