<?php
$organism = $variables['node']->organism;
$types    = $organism->feature_counts['types'];
$enabled  = $organism->feature_counts['enabled'];

if($enabled && count($types) > 0){ ?>
<div id="tripal_organism-feature_counts-box" class="tripal_organism-info-box tripal-info-box">
  <div class="tripal_organism-info-box-title tripal-info-box-title">Data Type Summary</div>
  <div class="tripal_organism-info-box-desc tripal-info-box-desc">The following data types are currently present for this organism</div>
     
  <table id="tripal_organism-table-feature_counts" class="tripal_organism-table tripal-table tripal-table-horz">     
    <tr class="tripal_organism-table-odd-row tripal-table-even-row">
      <th class="tripal-table-first-column">Feature Type</th>
      <th>Count</th>
    </tr> 
    <?php 
    $last = sizeof($types)-1;
    foreach ($types as $type){ 
      $class = 'tripal_organism-table-odd-row tripal-table-odd-row';
      if($i % 2 == 0 ){
        $class = 'tripal_organism-table-even-row tripal-table-even-row';
      }
      if ($i == $last) {
        $class .= ' tripal-table-last-row';
      }
      $i++;
      ?>
           
      <tr class="<?php print $class ?>">
        <td class="tripal-table-first-column"><span title="<?php print $type->definition ?>"><?php print $type->feature_type?></span></td>
        <td><?php print number_format($type->num_features) ?></td>
      </tr> 
           
      <?php } ?>
    </table>
    <img class="tripal_cv_chart" id="tripal_feature_cv_chart_<?php print $organism->organism_id?>" src="" border="0">
</div> 
<?php } ?>



