<?php
$feature = $variables['node']->feature;

// expand the feature object to include the libraries from the library_feature
// table in chado.
$feature = tripal_core_expand_chado_vars($feature,'table','library_feature');

// get the references. if only one reference exists then we want to convert
// the object into an array, otherwise the value is an array
$library_features = $feature->library_feature;
if (!$library_features) {
   $library_features = array();
} elseif (!is_array($library_features)) { 
   $library_features = array($library_features); 
}

if(count($library_features) > 0){ 
  
  // Libraries grouped by Type
  $libraries = array();
  foreach ($library_features as $library_feature){
    $libraries[ $library_feature->library_id->type_id->name ][] = $library_feature;
  }
?>

<div id="tripal_feature-libraries-box" class="tripal_feature-info-box tripal-info-box">
  <div class="tripal_feature-info-box-title tripal-info-box-title">Libraries</div>
  

    <?php
    $i = 0; 
    foreach ($libraries as $type => $type_libraries) {
    ?>
      <div class="tripal_feature-info-box-subtitle tripal-info-box-subtitle"><?php print ucwords(str_replace('_',' ',$type)); ?> Libraries</div>
      <ul class="tripal_feature-list tripal-list">
      <?php foreach ($type_libraries as $library_feature){ ?>
          <li><?php 
            if($library_feature->library_id->nid){
               print "<a href=\"". url("node/".$library_feature->library_id->nid) . "\">".$library_feature->library_id->name."</a>";
            } else {
               print $library_feature->library_id->name;
            }
          ?>
          </li>
          <table id="tripal_library-base-table" class="tripal_library-table tripal-table tripal-table-vert">
            <tr class="tripal_library-table-even-row tripal-table-even-row">
              <th nowrap>Unique Name</th>
              <td><?php print $library_feature->library_id->uniquename; ?></td>
            </tr>
            <tr class="tripal_library-table-even-row tripal-table-even-row">
              <th>Organism</th>
              <td>
                <?php if ($library_feature->library_id->organism_id->nid) { 
                 print "<a href=\"".url("node/".$library_feature->library_id->organism_id->nid)."\">".$library_feature->library_id->organism_id->genus ." " . $library_feature->library_id->organism_id->species ." (" .$library_feature->library_id->organism_id->common_name .")</a>";      	 
                } else { 
                  print $library_feature->library_id->organism_id->genus ." " . $library_feature->library_id->organism_id->species ." (" .$library_feature->library_id->organism_id->common_name .")";
                } ?>
              </td>
            </tr>      
            <tr class="tripal_library-table-odd-row tripal-table-odd-row">
              <th>Type</th>
              <td><?php 
                  if ($library_feature->library_id->type_id->name == 'cdna_library') {
                     print 'cDNA';
                  } else if ($library_feature->library_id->type_id->name == 'bac_library') {
                     print 'BAC';
                  } else {
                     print $library_feature->library_id->type_id->name;
                  }
                ?>
              </td>
            </tr>
          </table>
      <?php } ?>
      </ul>
    <?php } ?>
</div>
<?php }?>
