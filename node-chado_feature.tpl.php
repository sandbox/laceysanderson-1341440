<?php
// Feature Node Template
// Implements Long Index-Style Nodes

drupal_add_css('./tripal-node-templates.css');
$feature  = $variables['node']->feature;
?>

<?php if ($teaser) {
  include('tripal_feature/tripal_feature_teaser.tpl.php');
} else { ?>

<script type="text/javascript">
// Adds all tripal expandable boxes to the top bar
Drupal.behaviors.myBehavior = function (context) {
   $(document).ready(function() {

      // iterate through all of the info boxes and add their titles
      // to the table of contents
      $(".tripal-info-box-title").each(function(key, fn){
        var parent = $(this).parent();
        var id = $(parent).attr('id');
        var title = $(this).text();
        var lastIndex = $(".tripal-info-box-title").size() -1;
        if (key == lastIndex) {
          $('#tripal_feature-topbar-list').append('<li><a href="#'+id+'" class="tripal_feature-topbar-item">'+title+'</a></li>');
        } else {
          $('#tripal_feature-topbar-list').append('<li><a href="#'+id+'" class="tripal_feature-topbar-item">'+title+'</a> &middot; </li>');
        }
      });

      // when a title in the table of contents is clicked, then
      // scroll to the corresponding item
      $(".tripal_feature-topbar-item").click(function(){
         href = $(this).attr('href');
         document.$(href).scrollIntoView();
         return false;
      });

   });
}
</script>

<script type="text/javascript">
// Ensures the topbar always stays at the top of the screen
if (Drupal.jsEnabled) {
  $(document).ready(function() {

    // set the top section toolbar width based on it's placeholder
    $.setToolbarWidth = function() {
      var width = $(".tripal-topbar-placeholder").width();
      var inner = $(".inner").width();
      var paddingRight = $(".tripal-topbar").css('padding-right').replace("px", "");
      var paddingLeft = $(".tripal-topbar").css('padding-left').replace("px", "");
      var topbarWidth = width - paddingRight - paddingLeft;
      $(".tripal-topbar").css('width',topbarWidth);
    };
    var setToolbarWidth = $.setToolbarWidth();

    $.setToolbarTop = function() {
      var newTop = $(window).scrollTop();
      if (newTop > originalTop) {
        $(".tripal-topbar").css('top',0);
      } else if (newTop < originalTop) {
         bufferedTop = originalTop - newTop;
         $(".tripal-topbar").css('top',bufferedTop);
      }
    }

    $.setToolbarLeft = function () {
      var placeholderLeft = $(".tripal-topbar-placeholder").offset().left;
      var pageLeft = $(window).scrollLeft();
      var newToolbarLeft = placeholderLeft - pageLeft;
      $(".tripal-topbar").css('left',newToolbarLeft);
    }

  	// Make sure it's max-width is set to that of the page
  	$(window).resize(setToolbarWidth);

  	// Part that scrolls the div toolbar
    var originalTop = $(".tripal-topbar-placeholder").offset().top;
    $(window).scroll(function() {
      $.setToolbarWidth();
      $.setToolbarTop();
      $.setToolbarLeft();
    });
  });
}
</script>


<!-- Table of contents -->
<div class="tripal-topbar-placeholder">
  <div id="tripal_feature-topbar" class="tripal-topbar">
    <span id="tripal_feature-topbar-title" class="tripal-topbar-title">Sections:</span>
    <ul id="tripal_feature-topbar-list" class="tripal-topbar-list">
    </ul>
  </div>
</div>

<div id="tripal_feature-node" class="tripal-node">

   <!-- Basic Details Theme -->
   <?php include('tripal_feature/tripal_feature_base.tpl.php'); ?>

		<?php print $content ?>

   <!-- Database References -->
   <?php include('tripal_feature/tripal_feature_references.tpl.php'); ?>

   <!-- Properties -->
   <?php include('tripal_feature/tripal_feature_properties.tpl.php'); ?>

   <!-- Synonyms -->
   <?php include('tripal_feature/tripal_feature_synonyms.tpl.php'); ?>

   <!-- Sequence -->
   <?php
   if(strcmp($feature->type_id->name,'scaffold')!=0 and
      strcmp($feature->type_id->name,'chromosome')!=0 and
      strcmp($feature->type_id->name,'supercontig')!=0 and
      strcmp($feature->type_id->name,'pseudomolecule')!=0)
   {
      include('tripal_feature/tripal_feature_sequence.tpl.php');
   }
   ?>

   <!-- Relationships -->
   <?php include('tripal_feature/tripal_feature_relationships.tpl.php'); ?>

   <!-- Feature locations -->
   <?php
   if(strcmp($feature->type_id->name,'scaffold')!=0 and
      strcmp($feature->type_id->name,'chromosome')!=0 and
      strcmp($feature->type_id->name,'supercontig')!=0 and
      strcmp($feature->type_id->name,'pseudomolecule')!=0)
   {
      include('tripal_feature/tripal_feature_featurelocs.tpl.php');
   }
   ?>
</div>

<?php } ?>
